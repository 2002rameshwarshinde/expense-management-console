package expenseSignUp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import expenseDashboard.DashBoardScreen;
import expenseHome.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController {

	@FXML
	private TextField name;
	@FXML
	private TextField email;
	@FXML
	private TextField MobileNo;
	@FXML
	private PasswordField Password;
	@FXML
	private ComboBox currency;
	@FXML
	private ComboBox language;
	@FXML
	private ComboBox country;

	@FXML
	private Button back;

	@FXML
	private Button Signin;

	public void Signup(ActionEvent event) throws IOException {
		if (name.getText().isEmpty() || MobileNo.getText().isEmpty() || email.getText().isEmpty()
				|| Password.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please Fill All The Fields.");
			alert.showAndWait();
			return;
		}

		final String messageContent = "{\n" + "\"fullName\"" + ":\"" + name.getText() + "\", \r\n" + "\"email\"" + ":\""
				+ email.getText() + "\", \r\n" + "\"mobileNo\"" + ":\"" + MobileNo.getText() + "\", \r\n"
				+ "\"password\"" + ":\"" + Password.getText() + "\", \r\n" + "\"country\"" + ":\"" + country.getValue()
				+ "\", \r\n" + "\"currency\"" + ":\"" + currency.getValue() + "\", \r\n" + "\"language\"" + ":\""
				+ language.getValue() + "\" \r\n" + "\n}";


		System.out.println(messageContent);


		String url = "http://localhost:8080/directory/api/v1/signup";
		URL ur = new URL(url);
		HttpURLConnection Connection = (HttpURLConnection) ur.openConnection();
		Connection.setRequestMethod("POST");
		Connection.setRequestProperty("userId", "abcdef");

		Connection.setRequestProperty("Content-Type", "application/json");
		Connection.setDoOutput(true);

		OutputStream osObj = Connection.getOutputStream();
		osObj.write(messageContent.getBytes());

		osObj.flush();
		osObj.close();
		int respCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader irObj = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			Connection.disconnect();

			System.out.println(sb.toString());

		} else {

			System.out.println("POST Request did not work.");
		}
	}

	public void back(ActionEvent event) {
		new DashBoardScreen().show();
	}
}
