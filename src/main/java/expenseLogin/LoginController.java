package expenseLogin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import expenseDashboard.DashBoardScreen;
import expenseHome.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class LoginController {
	@FXML
	private TextField email;
	@FXML
	private TextField loginMobileNum;
	@FXML
	private TextField pass;

	@FXML
	private Button Login;

	@FXML
	private Button back;

	public void loginback(ActionEvent event) {

		new HomeScreen().show();
	}

	public void dashboard(ActionEvent event) throws IOException {

		if (email.getText().isEmpty() || loginMobileNum.getText().isEmpty() || pass.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all The fields.");
			alert.showAndWait();
			return;
		}

		final String messageContent = "{\n" + "\"email\"" + ":\"" + email.getText() + "\", \r\n" + "\"mobile\"" + ":\""
				+ loginMobileNum.getText() + "\", \r\n" + "\"password\"" + ":\"" + pass.getText() + "\" \r\n" + "\n}";

		System.out.println(messageContent);

		String url = "http://localhost:8080/directory/api/v1/validate";
		URL ur = new URL(url);
		HttpURLConnection Connection = (HttpURLConnection) ur.openConnection();
		Connection.setRequestMethod("POST");
		Connection.setRequestProperty("userId", "abcdef");

		Connection.setRequestProperty("Content-Type", "application/json");
		Connection.setDoOutput(true);

		OutputStream os = Connection.getOutputStream();
		os.write(messageContent.getBytes());

		os.flush();
		os.close();
		int respCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputst = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(inputst);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			Connection.disconnect();

			System.out.println(sb.toString());
			new DashBoardScreen().show();

		} else {

			System.out.println("POST Request did not work.");

		}

	}

}
