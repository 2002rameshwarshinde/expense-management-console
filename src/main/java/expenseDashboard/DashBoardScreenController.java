package expenseDashboard;

import expenseCreateFriend.CreateFriend;
import expenseCreateGroup.CreateGroup;
import expenseHome.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class DashBoardScreenController {

	@FXML
	private Button createfriend;
	
	@FXML
	private Button creategroup;
	
	@FXML
	private Button back;
	
	public void createfriend(ActionEvent event) {
		new CreateFriend().show();
	}
	
	public void creategroup(ActionEvent event) {
		new CreateGroup().show();
	}
	
	public void back(ActionEvent event) {
		new HomeScreen().show();
	}
}
