package application;

import expenseHome.HomeScreen;
import expenseLogin.LoginExpense;
import expenseSignUp.SignUp;
import javafx.application.Application;
import javafx.stage.Stage;
import stageMaster.StageMaster;

public class ApplicationMain extends Application {
	public static void main(String args[]) {

		launch(args);

	}

	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
	 	new HomeScreen().show();
	
		
	}
}
