package expenseHome;



import expenseLogin.LoginExpense;
import expenseSignUp.SignUp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class HomeScreenController {
     @FXML
	private Button signupbutton;
	@FXML
	private Button loginbutton; 
	
	
	
	public void Newsignup(ActionEvent event) {
		new SignUp().show();
	}
	public void Newlogin(ActionEvent event) {
		new LoginExpense().show();
	}
}
