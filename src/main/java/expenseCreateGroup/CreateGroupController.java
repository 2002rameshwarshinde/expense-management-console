package expenseCreateGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import expenseDashboard.DashBoardScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class CreateGroupController {
  @FXML
	private TextField groupName;
	
  @FXML
  private TextField groupType;
  
  @FXML
  private Button createGroup;
  
  @FXML
  private Button back;
  
  
  public void DashboardScreen(ActionEvent event) throws IOException {
	  
	  if (groupName.getText().isEmpty() || groupType.getText().isEmpty() ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all The fields.");
			alert.showAndWait();
			return;
		}

	  final String messageContent = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"groupType\"" + ":\""
				+ groupType.getText() +  "\" \r\n"+ "\n}";
		
		System.out.println(messageContent);

		String url = "http://localhost:8080/groups/api/v1/create";
		URL ur = new URL(url);
		HttpURLConnection Connection = (HttpURLConnection) ur.openConnection();
		Connection.setRequestMethod("POST");
		Connection.setRequestProperty("userId", "abcdef");
		
		Connection.setRequestProperty("Content-Type", "application/json");
		Connection.setDoOutput(true);
		
		OutputStream os = Connection.getOutputStream();
		os.write(messageContent.getBytes());
		
		os.flush();
		os.close();
		int respCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		
		if (respCode == HttpURLConnection.HTTP_CREATED) {
	
			InputStreamReader irObj = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			Connection.disconnect();
		
			 
			System.out.println(sb.toString());
			
			 new DashBoardScreen().show();
		} else {
		
			System.out.println("POST Request did not work.");
					
	}

	
  }
  public void back (ActionEvent event) {
	  new DashBoardScreen().show();
  }
}
