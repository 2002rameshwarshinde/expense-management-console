 package expenseCreateFriend;

import expenseDashboard.DashBoardScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class CreateFriendController {
@FXML
	private TextField fullname;
@FXML
	private TextField email;
@FXML
	private TextField mobNo;

@FXML
private Button createFriend;

@FXML
private Button back;

public void back(ActionEvent event) {
	new DashBoardScreen().show();
}

}
